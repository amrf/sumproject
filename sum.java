import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class sum {
    static int counter = 0;
    static int maxNumber = 0;
    static int secondMaxNumber = 0;

    public static void main(String[] args) {
        Scanner sc = null;

        try {
            sc = new Scanner(new FileReader("C:/Users/alfredo.rangel/Documents/study/arrayFile.txt"));
        } catch (FileNotFoundException e) {
            // do something with e, or handle this case
        }
        while (sc.hasNextLine()) {
            counter = 0;
            maxNumber = 0;
            secondMaxNumber = 0;
            sumIntegerArray(getIntArray(sc.nextLine()));
            int finalTotal = maxNumber + secondMaxNumber;
            System.out.println("Resultado: " + maxNumber + " + " + secondMaxNumber + " = " + finalTotal);
        }
    }


    //Method created to create an array of integer from an String;
    private static int[] getIntArray(String line) {

        String[] parts = line.split(",");
        int[] integerArray = new int[parts.length];

        for (int i = 0; i < parts.length; i++) {
            int finalNumber = Integer.parseInt(parts[i]);
            integerArray[i] = finalNumber;
        }
        return integerArray;
    }

    //Method to make the sum recursivelly
    private static void sumIntegerArray(int[] integerArray) {
        int z = counter;

        if (counter < integerArray.length) {

            int i = integerArray[z];

            counter++;

            if (verifyIfIsGreaterThan(i, maxNumber)) {
                secondMaxNumber = maxNumber;
                maxNumber = i;
            }

            if (verifyIfIsGreaterThan(i, secondMaxNumber) && i != maxNumber) {
                secondMaxNumber = i;
            }

            // Recursion
            sumIntegerArray(integerArray);
        }
    }

    private static boolean verifyIfIsGreaterThan(int number, int max) {
        boolean booleanToReturn = false;
        if (number > max) {
            booleanToReturn = true;
        }
        return booleanToReturn;
    }

}