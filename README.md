# sumProject

This project has the objective to study recursion with java. 

It reads lines from a file, generate an Integer array and search two numbers in that array whose sum is the largest among all possible sums.

It has diverse possible ways of resolution, we can work with loop or with recursion.

I choose recursion to practice more the language, since I'm studying functional programming, I think that the best is practicing what the FP 
says.